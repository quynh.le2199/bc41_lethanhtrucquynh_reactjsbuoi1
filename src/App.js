import logo from './logo.svg';
import './App.css';import Navigation from './BaiTapLayoutComponent/Navigation';
import Footer from './BaiTapLayoutComponent/Footer';
import Header from './BaiTapLayoutComponent/Header';
import PageContent from './BaiTapLayoutComponent/PageContent';
import BaiTapThucHanhLayout from './BaiTapLayoutComponent/BaiTapThucHanhLayout';

function App() {
  return (
    <div className="App">
      <BaiTapThucHanhLayout/>
    </div>
  );
}

export default App;
